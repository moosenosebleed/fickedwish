//
//  Report.swift
//  
//
//  Created by Speedy Gonzalez on 12/14/16.
//
//

import Foundation
import Firebase

struct report {
    let hall: String
    let floor: Int
    let time: String
}
