//
//  RACell.swift
//  fickedwish
//
//  Created by Speedy Gonzalez on 11/16/16.
//  Copyright © 2016 Default. All rights reserved.
//

import UIKit

class RACell: UITableViewCell {
    
    //Add some labels and stuff
    
    @IBOutlet weak var locationLabel : UILabel!
    
    @IBOutlet weak var dateLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
