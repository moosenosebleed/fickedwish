//
//  MapVC.swift
//  fickedwish
//
//  Created by Speedy Gonzalez on 11/16/16.
//  Copyright © 2016 Default. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController {
    
    let halls = ["Ketler East Gable",
                 "Ketler West Gable",
                 "Ketler East Terrace",
                 "Ketler Central Terrace",
                 "Ketler West Terrace",
                 "Lincoln Zerbe Side",
                 "Lincoln Ketler Side",
                 "Lincoln Middle",
                 "Hopeman Zerbe Side",
                 "Hopeman Buhl Side",
                 "Memorial Crawford Side",
                 "Memorial President Side",
                 "Hicks Buhl Side",
                 "Hicks Pew Side",
                 "Hicks Middle",
                 "MAP South",
                 "MAP West",
                 "MAP North",
                 "Harker",
                 "MEP"]
    @IBOutlet weak var map : MKMapView!
    
    var reportTab: ReportVC!
    
    dynamic var lat : Double = 41.1553798 //41.1562038 //Grove City
    dynamic var long : Double = -80.0807378 //Grove City
    
    var reports = [Report]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Change the map to a hybrid flyover satellite map type.
        let sat : MKMapType = MKMapType(rawValue: 4 )!
        map.mapType = sat
        
        var loc = CLLocationCoordinate2D(latitude: lat,   longitude: long)
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let reg = MKCoordinateRegion(center: loc, span: span)
        map.setRegion(reg, animated: false)
        
        
        
        
        
        //Ok, now the stuff below needs to be done for each pin.
        reports.sort{ (a, b) -> Bool in
            a.time > b.time
        }
        for i in 0...10{
            
            
            var templat=0.0
            var templon = 0.0
            //loc = CLLocationCoordinate2D(latitude: loc.latitude+0.0001, longitude: loc.longitude+0.0001)
            switch reports[i].hall {
            
            case halls[0],halls[1],halls[2],halls[3],halls[4]: //ketler
                templat = 41.1553798
                templon = -80.0807378
            case halls[5],halls[6],halls[7]: //lincoln
                templat = 41.1546786
                templon = -80.0806191
            case halls[8],halls[9]:     //hopeman
                templat = 41.1540935
                templon = -80.0804547
            case halls[10],halls[11]:     //memorial
                templat = 41.1550009
                templon = -80.0820783
            case halls[12],halls[13],halls[14]: //hicks
                templat = 41.1535953
                templon = -80.0783098
            case halls[15],halls[17],halls[17]: //MAP
                templat = 41.1563943
                templon = -80.079865
            case halls[18]:             //harker
                templat = 41.155977
                templon = -80.0791155
            case halls[19]:             //mep
                templat = 41.1565893
                templon = -80.0784964
            
            
            default:
                templat=lat
                templon=long
            }
            loc = CLLocationCoordinate2D(latitude: templat, longitude: templon)

            let tempPin = MKPointAnnotation()
            tempPin.coordinate = loc
            tempPin.title = reports[i].time + " " + reports[i].hall
        
            map.addAnnotation(tempPin)
        }
                // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
