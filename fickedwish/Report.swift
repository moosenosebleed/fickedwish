//
//  Report.swift
//  
//
//  Created by Speedy Gonzalez on 12/14/16.
//
//

import Foundation
import Firebase

struct Report {
    let key: String
    let hall: String
    let floor: Int
    let time: String
    let ref: FIRDatabaseReference?
    
    init(hall: String, floor: Int, time: String, key: String = "") {
        self.key = key
        self.hall = hall
        self.floor = floor
        self.time = time
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        hall = snapshotValue["hall"] as! String
        floor = snapshotValue["floor"] as! Int
        time = snapshotValue["time"] as! String
        ref = snapshot.ref
    }
    
    func toAnyObject() -> Any {
        return [
            "hall": hall,
            "floor": floor,
            "time": time
        ]
    }
    
    
}
