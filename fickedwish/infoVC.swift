//
//  infoVC.swift
//  fickedwish
//
//  Created by Speedy Gonzalez on 12/15/16.
//  Copyright © 2016 Default. All rights reserved.
//

import UIKit

class infoVC: UIViewController {
    
    @IBOutlet weak var gccSite : UIWebView!
    @IBOutlet weak var gccImg : UIImageView!
    
    var image : UIImage? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // MARK: Website stuff
        
        let gccUrl = NSURL(string: "https://en.wikipedia.org/wiki/Grove_City_College")
        let request = NSURLRequest(url: gccUrl as! URL)
        gccSite.loadRequest(request as URLRequest)
        
        
        
        // MARK: Data downloading stuff
        
        let url = "http://www.gcc.edu/_layouts/GCC/Images/mobile/header_logo.png"
        
        let queue = OperationQueue()
        queue.name = "Pictures"
        
        //Downloading the gcc image on a separate thread.
        let op = ImgOp(urlString: url)
        op.completionBlock = {
            OperationQueue.main.addOperation {
                self.image = op.img!
                self.gccImg.image = self.image
            }
        }
        queue.addOperation(op)
 
 

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
