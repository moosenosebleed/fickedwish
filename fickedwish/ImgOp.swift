//
//  ImgOp.swift
//  Program5
//
//  Created by Speedy Gonzalez on 11/9/16.
//  Copyright © 2016 Andrew Gonzalez. All rights reserved.
//

import UIKit

class ImgOp: Operation {
    
    var url : String = ""
    var img : UIImage? = nil
    //var index : Int = 0
    
    init(urlString: String) {
        url = urlString
        //index = i
    }
    
    override func main() {
        let imgUrl = URL(string: url)
        let data = try? Data(contentsOf: imgUrl!)
        if (data != nil) {
        img = UIImage(data: data!)
        }
    }
}
