//
//  ReportsRepository.swift
//  fickedwish
//
//  Created by Speedy Gonzalez on 11/16/16.
//  Copyright © 2016 Default. All rights reserved.
//

import Foundation

class ReportsRepository: NSObject {
    var reports = [Report]()
    
    //Make this a Singleton class
    static let sharedInstance: ReportsRepository = {
        let instance = ReportsRepository()
        return instance
    }()
}
