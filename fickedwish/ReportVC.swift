//
//  ReportVC.swift
//  fickedwish
//
//  Created by Speedy Gonzalez on 11/16/16.
//  Copyright © 2016 Default. All rights reserved.
//

import UIKit
import Firebase

class ReportVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var floorControl : UISegmentedControl!
    
    @IBOutlet weak var hallPicker : UIPickerView!
    
    
    let date=DateFormatter()
    var hallString : String = "Ketler East Gable"
    var mapTab: MapVC!
    
    let halls = ["Ketler East Gable",
                 "Ketler West Gable",
                 "Ketler East Terrace",
                 "Ketler Central Terrace",
                 "Ketler West Terrace",
                 "Lincoln Zerbe Side",
                 "Lincoln Ketler Side",
                 "Lincoln Middle",
                 "Hopeman Zerbe Side",
                 "Hopeman Buhl Side",
                 "Memorial Crawford Side",
                 "Memorial President Side",
                 "Hicks Buhl Side",
                 "Hicks Pew Side",
                 "Hicks Middle",
                 "MAP South",
                 "MAP West",
                 "MAP North",
                 "Harker",
                 "MEP"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //Setting properties for the UIPickerView
        hallPicker.dataSource = self
        hallPicker.delegate = self
        
        date.dateFormat="MM/dd hh:mm"
 
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return halls.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return halls[row]
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 300
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        hallString = halls[row]
    }
    
    
    @IBAction func submit() {
        
        // MARK: File stuff
        let fm = FileManager.default
        
        //let startingKey = "0"
        
        //path for library.
        let libPath : String = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0]
        
        //Add specific file path
        let libUrl = URL(fileURLWithPath: libPath)
        let fullPath = libUrl.appendingPathComponent("lastKey.txt")
        
        //Print the path so we can find the file if we need it.
        print(libPath)
        
        //Certain functions only work with strings, not URLS, so we convert fullPath to string
        var strFullPath = fullPath.absoluteString
        
        //This will hold what's currently in the file
        var lastKey : String = ""
        
        //Get rid of "file://" from the beginning of the fullPath string
        let bottom = strFullPath.characters.startIndex
        let top = strFullPath.characters.index(bottom, offsetBy: 6)
        strFullPath.removeSubrange(bottom...top)
        
        //Read from the file
        if (fm.fileExists(atPath: strFullPath)) {
            
            do {
                lastKey = try String(contentsOfFile: strFullPath)
            }
            catch {
                print("Error: Couldn't read from the lastKey.txt file in ReportVC.")
            }
            
        } else {

            print("Error: Couldn't find lastKey.txt file in ReportVC.")
        }
        
        print("lastKey = \(lastKey)")
        
        //Convert lastKey to an Int, increment it, then make it a string again as nextKey.
        let lastKeyInt = Int(lastKey)
        
        print("lastKey is \(lastKeyInt!)") //Testing purposes
        
        let nextKeyInt = lastKeyInt! + 1
        
        let nextKey = "\(nextKeyInt)"
        
        let now = Date.init(timeIntervalSinceNow: 0)
    
        
        //testing
        print(hallString)
        
 
        // MARK: Firebase stuff
        
        //This is a reference to the reportings Firebase database
        let ref = FIRDatabase.database().reference(withPath: "reportings")
        
        //Here, we make a report object and fill it with what the user inputted
        let newReport = Report(hall: self.hallString, floor: (self.floorControl.selectedSegmentIndex + 1), time: self.date.string(from: now), key: nextKey)
                
        //Make a new child reference based off of ref
        let newReportRef = ref.child(nextKey)
        
        //Add the new report to the child ref
        newReportRef.setValue(newReport.toAnyObject())
        
        
        // MARK: Writing the next index to the file
        
        do {
            try nextKey.write(to: fullPath, atomically: true, encoding: .utf8)
        }
        catch {
            print("Error: Couldn't write to lastKey.txt in ReportVC.")
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
