//
//  RATable.swift
//  fickedwish
//
//  Created by Speedy Gonzalez on 11/16/16.
//  Copyright © 2016 Default. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

class RATable: UITableViewController {
    
    var row : Int = 0
    
    let listToInfo = "ListToInfo"
    
    //The user's notification preferences
    var notifPref = ""
    
    let allReports = ReportsRepository.sharedInstance
    
    var userCountBarButtonItem: UIBarButtonItem!
    
    //Value initially set to true so that we don't get notifications on startup.
    var isOldReport : Bool = true
    
    var isStartup : Bool = true
    
    var hallString = ""
    
    var theDevice : UIDevice!
    
    //The inverse index. We use this to display the list in inverse chronological order.
    var invIndex = 0
    
    //Used to store the key that this load should start with
    var startKey = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        theDevice = UIDevice.current
        
        
        
        self.navigationItem.title = "Reportings"
        
        // MARK: Firebase stuff
        let ref = FIRDatabase.database().reference(withPath: "reportings")
        let prefRef = FIRDatabase.database().reference(withPath: "notifPreferences")
        
        //Listens for any changes (add, remove, change) to the database and updates the table immediately
        ref.observe(.value, with: { snapshot in
            var newReports: [Report] = []
            for report in snapshot.children {
                let newReport = Report(snapshot: report as! FIRDataSnapshot)
                newReports.append(newReport)
                
                let snapReport = report as! FIRDataSnapshot
                let snapReportVal = snapReport.value as! [String: AnyObject]
                
                // MARK: Local notification for specified hall
                    
                
                //Make sure that we only consider new entries
                self.isOldReport = false //reset value
                for report in self.allReports.reports {
                    if(report.key == snapReport.key) { //If any key in the current array == the snapshot key
                        self.isOldReport = true
                    }
                }
                
                //Set up a notification if the hall name matches the user's notification preference.
                if ((snapReportVal["hall"] as! String) == self.notifPref) && !self.isOldReport {
                    print("🍎 NotifPref block fired!")
                    
                    var notifHallString = (snapReportVal["hall"] as! String)
                    
                    //Append the floor to the hall name.
                    switch (snapReportVal["floor"] as! Int) {
                    case 1:
                        notifHallString = notifHallString + " 1st Floor"
                    case 2:
                        notifHallString = notifHallString + " 2nd Floor"
                    case 3:
                        notifHallString = notifHallString + " 3rd Floor"
                    case 4:
                        notifHallString = notifHallString + " 4th Floor"
                    default:
                        print("invalid floor")
                    }
                    
                    let note = UNMutableNotificationContent()
                    note.title = "RA Sighted"
                    note.body = "\(notifHallString) \(snapReportVal["time"] as! String)"
                    note.categoryIdentifier = "hallNotify"
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                    let request = UNNotificationRequest(identifier: "hallNote", content: note, trigger: trigger)
                    let center = UNUserNotificationCenter.current()
                    center.add(request, withCompletionHandler: {(error) in
                        if error != nil {
                            print("NOTIFICATION ERROR")
                        }
                        
                    })
                }
            //}
            }
            self.allReports.reports = newReports
            self.tableView.reloadData()
            //self.isOldReport = false //reset value

            
        })
        
        
         //Each time the app loads, pull the user's notification preferences from the database
         prefRef.observeSingleEvent(of: .value, with: { snapshot in
            for preference in snapshot.children {
                let snapPref = preference as! FIRDataSnapshot
                if snapPref.key == self.theDevice.name {
                    self.notifPref = snapPref.value as! String
                }
            }
         })
        
        //Each time the app loads, figure out which key value to start at.
        ref.observeSingleEvent(of: .value, with: {snapshot in
            for report in snapshot.children {
                let snapPref = report as! FIRDataSnapshot
                print("snapPref.key = \(snapPref.key)")
                print("startKey = \(self.startKey)")
                
                //We want to grab the greatest existing key
                if Int(snapPref.key)! > self.startKey {
                    self.startKey = Int(snapPref.key)!
                }
            }
            print("🍏 startKey = \(self.startKey)")
            
            //MARK: Writing the starting key to the library file
            
            //path for library.
            let libPath : String = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0]
            
            //Add specific file path
            let libUrl = URL(fileURLWithPath: libPath)
            let fullPath = libUrl.appendingPathComponent("lastKey.txt")
            
            //Certain functions only work with strings, not URLS, so we convert fullPath to string
            var strFullPath = fullPath.absoluteString
            
            let startKeyString = "\(self.startKey)"
            
            //Get rid of "file://" from the beginning of the fullPath string
            let bottom = strFullPath.characters.startIndex
            let top = strFullPath.characters.index(bottom, offsetBy: 6)
            strFullPath.removeSubrange(bottom...top)
            
            //Read from the file
            do {
                print("🍏 Start key to write: \(self.startKey)")
                try startKeyString.write(to: fullPath, atomically: true, encoding: .utf8)
            }
            catch {
                print("🍏 Start key that didn't write: \(self.startKey)")
                print("Error: Couldn't write to lastKey.txt in RATable.")
            }
            
        })
 
    }
    
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // ADD STUFF HERE
        return allReports.reports.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RACell
        
        invIndex = allReports.reports.endIndex - (indexPath.row + 1)

        hallString = allReports.reports[invIndex].hall
        
        //Append the floor to the hall name.
        switch allReports.reports[invIndex].floor {
        case 1:
            hallString = hallString + " 1st Floor"
        case 2:
            hallString = hallString + " 2nd Floor"
        case 3:
            hallString = hallString + " 3rd Floor"
        case 4:
            hallString = hallString + " 4th Floor"
        default:
            print("invalid floor")
        }

        cell.locationLabel.text = hallString
        cell.dateLabel.text = allReports.reports[invIndex].time

        return cell
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        row = indexPath.row
        
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true) //Makes things pretty
    }
    
    @IBAction func cogDidTouch(_ sender: AnyObject) {
        
        //This function presents an alert for the user to receive local notifications.
        let alert = UIAlertController(title: "Notifications",
                                      message: "Enter a hall to receive notifications for",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save",
                                       style: .default) { action in
                                        let textField = alert.textFields![0]
                                        self.notifPref = textField.text!
                                        
                                        // MARK: Firebase stuff
                                        
                                        //This is a reference to the notifications Firebase database
                                        let prefRef = FIRDatabase.database().reference(withPath: "notifPreferences")
                                        
                                        //Make a new child reference based off of prefRef, using the device name as a key
                                        let newPrefRef = prefRef.child(self.theDevice.name)
                                        
                                        //Add the new preference to the child ref
                                        newPrefRef.setValue(textField.text!)

        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default)
        
        alert.addTextField()
        
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func submit (segue: UIStoryboardSegue) {
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="toMap"{
            let dest = segue.destination as! MapVC
            dest.reports=self.allReports.reports
        }
    }

}
